var ejs = require( 'ejs' );
var server = require( 'webserver' ).create();

var listen_port = 7771;
var output_width = 450;
var output_height = 600;

var service = server.listen( listen_port, function( request, response ) {
  var page = new WebPage();
  page.content = ejs.render( request.post.template, request.post );
  page.viewportSize = { width: output_width, height: output_height };
  page.clipRect = { top: 0, left: 0, width: output_width, height: output_height };

  // write http response
  response.statusCode = 200;
  response.write( page.renderBase64( 'PNG' ) );
  response.close()

  page.close();
} );

