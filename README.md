# Image Server Proof-of-Concept

## Introduction
This is a PhantomJS script that runs an HTTP server on port 7771. Requests take the form of an EJS template (https://github.com/visionmedia/ejs) and one or more other fields that will be automatically mapped during the template rendering process.


## Requirements
- PhantomJS (http://phantomjs.org/)
- EJS (npm install ejs)
- PHP

## Usage
The server can be started without any command line arguments:
./phantomjs image_server.js

An example PHP script is included that sends some data to the server and displays the response as an image:
php example.php
