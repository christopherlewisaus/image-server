<?php

// like pecl_http but without having to try and install the stupid extension
function http_post_flds($url, $data, $headers=null) {   
    $data = http_build_query($data);    
    $opts = array('http' => array('method' => 'POST', 'content' => $data));

    if($headers) {
        $opts['http']['header'] = $headers;
    }
    $st = stream_context_create($opts);
    $fp = fopen($url, 'rb', false, $st);

    if(!$fp) {
        return false;
    }
    return stream_get_contents($fp);
}

$server = "http://localhost:7771";
$fields = array( 
  'template' => '<html><body><%- content %></body></html>',
  'content' => '<h1>Hello World</h1><p>This is me.</p>'
);

$response = http_post_flds( $server, $fields );

?>

<html>
  <img src="data:image/png;base64,<?= $response ?>" />
</html>
